package com.usl.dynamock.repository;

import com.usl.dynamock.data.DynaForm;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynaFormRepository extends MongoRepository<DynaForm, String> {
}
