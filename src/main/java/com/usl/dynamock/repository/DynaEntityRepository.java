package com.usl.dynamock.repository;

import com.usl.dynamock.data.DynaEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynaEntityRepository extends MongoRepository<DynaEntity, String> {
}
