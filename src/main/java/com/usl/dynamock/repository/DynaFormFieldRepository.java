package com.usl.dynamock.repository;

import com.usl.dynamock.data.DynaFormField;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynaFormFieldRepository extends MongoRepository<DynaFormField, String> {
}
