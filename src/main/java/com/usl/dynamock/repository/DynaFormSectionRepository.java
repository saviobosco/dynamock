package com.usl.dynamock.repository;

import com.usl.dynamock.data.DynaFormSection;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynaFormSectionRepository extends MongoRepository<DynaFormSection, String> {
}
