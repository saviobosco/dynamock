package com.usl.dynamock.repository;

import com.usl.dynamock.data.DynaField;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynaFieldRepository extends MongoRepository<DynaField, String> {
}
