package com.usl.dynamock.repository;

import com.usl.dynamock.data.DynaFormAction;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynaFormActionRepository extends MongoRepository<DynaFormAction, String> {
}
