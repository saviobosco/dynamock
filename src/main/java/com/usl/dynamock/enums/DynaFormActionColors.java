package com.usl.dynamock.enums;

public enum DynaFormActionColors {
    DANGER,
    PRIMARY,
    SUCCESS,
    INFO,
    CUSTOM
}
