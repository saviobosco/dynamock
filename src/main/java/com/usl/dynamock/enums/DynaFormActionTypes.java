package com.usl.dynamock.enums;

public enum DynaFormActionTypes {

    SAVE_OR_UPDATE("SAVE_OR_UPDATE", "save to the in-house database"),
    CANCEL("CANCEL", "Cancel the form"),
    CALL_EXT_SERVICE("CALL_EXT_SERVICE", "submit form to an external service"),
    CALL_INT_SERVICE("CALL_INT_SERVICE", "submit form to an internal service");

    private final String title;
    private final String description;

    DynaFormActionTypes(String typeTitle, String typeDescription)
    {
        title = typeTitle;
        description = typeDescription;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
