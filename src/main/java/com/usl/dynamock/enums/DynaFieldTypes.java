package com.usl.dynamock.enums;

public enum DynaFieldTypes {
    TEXT,
    NUMBER,
    BOOLEAN,
    DECIMAL,
    MONEY,
    ENTITY,
    ENTITY_LIST

}
