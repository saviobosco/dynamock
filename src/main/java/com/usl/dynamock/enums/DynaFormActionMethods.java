package com.usl.dynamock.enums;

public enum DynaFormActionMethods {
    POST,
    GET,
    PUT,
    DELETE,
    PATCH
}
