package com.usl.dynamock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynaMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynaMockApplication.class, args);
	}

}
