package com.usl.dynamock.validation;

import com.usl.dynamock.enums.DynaFieldTypes;
import com.usl.dynamock.enums.constraints.ValueOfEnum;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

public class DynaEntityFieldValidation implements Serializable {

    @NotEmpty(message = "Name can not be empty")
    private String name;

    @NotEmpty(message = "fieldType can not be empty")
    //@ValueOfEnum(enumClass = DynaFieldTypes.class)
    private String fieldType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    @Override
    public String toString() {
        return "DynaEntityFieldValidation{" +
                "name='" + name + '\'' +
                ", fieldType='" + fieldType + '\'' +
                '}';
    }
}
