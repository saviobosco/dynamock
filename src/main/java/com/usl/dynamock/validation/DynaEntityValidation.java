package com.usl.dynamock.validation;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

public class DynaEntityValidation implements Serializable {

    @NotEmpty(message = "Name can not be empty")
    private String name;

    @NotEmpty(message = "Description can not be empty")
    private String description;

    @Valid
    private List<DynaEntityFieldValidation> dynaFields;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<DynaEntityFieldValidation> getDynaFields() {
        return dynaFields;
    }

    public void setDynaFields(List<DynaEntityFieldValidation> dynaFields) {
        this.dynaFields = dynaFields;
    }

    @Override
    public String toString() {
        return "DynaEntityValidation{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dynaFields=" + dynaFields +
                '}';
    }
}
