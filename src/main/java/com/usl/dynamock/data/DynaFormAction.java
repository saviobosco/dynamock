package com.usl.dynamock.data;

import com.usl.dynamock.enums.DynaFormActionColors;
import com.usl.dynamock.enums.DynaFormActionMethods;
import com.usl.dynamock.enums.DynaFormActionTypes;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dyna_form_actions")
@Getter
@Setter
@NoArgsConstructor
public class DynaFormAction {

    @Id
    private String id;

    private String actionTitle;

    private DynaFormActionColors colorScheme;

    private String colorString; // populated when the colorscheme is CUSTOM

    // SAVE_OR_UPDATE, CALL_EXT_SERVICE, CALL_INT_SERVICE
    private DynaFormActionTypes actionType; // type of action to perform

    // Url for the external action.
    private String actionUrl;

    // DynaService (when action = CALL_EXT_SERVICE ) When the actions is an external service.
    private DynaFormActionMethods actionMethod; // POST, GET, PUT, DELETE

    // DynaService (when action = CALL_INT_SERVICE )
    private String actionService;

    @Override
    public String toString() {
        return "DynaFormAction{" +
                "id='" + id + '\'' +
                ", actionTitle='" + actionTitle + '\'' +
                ", colorScheme=" + colorScheme +
                ", colorString='" + colorString + '\'' +
                ", actionType=" + actionType +
                ", actionUrl='" + actionUrl + '\'' +
                ", actionMethod=" + actionMethod +
                ", actionService='" + actionService + '\'' +
                '}';
    }
}
