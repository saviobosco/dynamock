package com.usl.dynamock.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "dyna_form_sections")
@Getter
@Setter
@NoArgsConstructor
public class DynaFormSection {

    @Id
    private String id;

    private String title;

    private String subtitle;

    @DBRef
    private List<DynaFormField> dynaFormFields;

    @Override
    public String toString() {
        return "DynaFormSection{" +
                "title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", dynaFormFields=" + dynaFormFields +
                '}';
    }

    public void addFormField(DynaFormField dynaFormField)
    {
        // add the dynaField to the list;
        if (dynaFormFields == null) {
            dynaFormFields = new ArrayList<>();
        }
        dynaFormFields.add(dynaFormField);
    }

    public void clearFormFields()
    {
        dynaFormFields = new ArrayList<>();
    }
}
