package com.usl.dynamock.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "dyna_entities")
@Getter
@Setter
@NoArgsConstructor
public class DynaEntity {

    @Id
    public String id;

    private String name;

    private String description;

    @DBRef
    private List<DynaField> dynaFields;

    public DynaEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    // Add a dynaField to the entity
    public void addField(DynaField dynaField)
    {
        // add the dynaField to the list;
        if (dynaFields == null) {
            dynaFields = new ArrayList<>();
        }
        dynaFields.add(dynaField);
    }

    @Override
    public String toString() {
        return "DynaEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dynaFields=" + dynaFields +
                '}';
    }

    public void clearDynaFields()
    {
        dynaFields = new ArrayList<>();
    }
}
