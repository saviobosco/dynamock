package com.usl.dynamock.data;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dyna_form_fields")
@Getter
@Setter
@NoArgsConstructor
public class DynaFormField {

    @Id
    private String id;

    // Related Dyna Field
    private DynaField dynaField;

    private Boolean required;

    private Boolean editable;

    private String title;

    private String placeHolder;

    private String hint;

    // other fields conditions will come below here
    // dependent upon some parameters.

    @Override
    public String toString() {
        return "DynaFormField{" +
                "id='" + id + '\'' +
                ", dynaField=" + dynaField +
                ", required=" + required +
                ", editable=" + editable +
                ", title='" + title + '\'' +
                ", placeHolder='" + placeHolder + '\'' +
                ", hint='" + hint + '\'' +
                '}';
    }
}
