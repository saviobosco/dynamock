package com.usl.dynamock.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "dyna_forms")
@Getter
@Setter
@NoArgsConstructor
public class DynaForm {

    @Id
    private String id;

    private String title;

    private String subTitle;

    // Related Entity Record
    @DBRef
    private DynaEntity dynaEntity;

    // Related Form Sections
    @DBRef
    private List<DynaFormSection> sections;

    // Related Form Actions to be performed
    @DBRef
    private List<DynaFormAction> actions;

    public void addFormSection(DynaFormSection dynaFormSection)
    {
        // add the dynaField to the list;
        if (sections == null) {
            sections = new ArrayList<>();
        }
        sections.add(dynaFormSection);
    }

    public void clearFormSections()
    {
        sections = new ArrayList<>();
    }

    public void addFormAction(DynaFormAction dynaFormAction)
    {
        // add the dynaField to the list;
        if (actions == null) {
            actions = new ArrayList<>();
        }
        actions.add(dynaFormAction);
    }

    public void clearFormActions()
    {
        actions = new ArrayList<>();
    }


    @Override
    public String toString() {
        return "DynaForm{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", dynaEntity=" + dynaEntity +
                ", sections=" + sections +
                ", actions=" + actions +
                '}';
    }
}
