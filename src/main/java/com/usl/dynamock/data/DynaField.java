package com.usl.dynamock.data;

import com.usl.dynamock.enums.DynaFieldTypes;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "dyna_fields")
@Getter
@Setter
@NoArgsConstructor
public class DynaField {

    @Id
    public String id;

    private String name;

    private DynaFieldTypes fieldType;

    // If the DynaFieldType = ENTITY OR ENTITY_LIST
    private DynaEntity dynaEntity;


    public DynaField(String name, DynaFieldTypes fieldType) {
        this.name = name;
        this.fieldType = fieldType;
    }

    @Override
    public String toString() {
        return "DynaField{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", fieldType=" + fieldType +
                ", dynaEntity=" + dynaEntity +
                '}';
    }
}
