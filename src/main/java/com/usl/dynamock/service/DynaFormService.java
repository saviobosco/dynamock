package com.usl.dynamock.service;

import com.usl.dynamock.data.DynaForm;
import com.usl.dynamock.repository.DynaFormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DynaFormService implements CrudService<DynaForm, String> {

    @Autowired
    DynaFormRepository dynaFormRepository;

    @Override
    public List<DynaForm> findAll() {
        return dynaFormRepository.findAll();
    }

    @Override
    public DynaForm findById(String id) {
        Optional<DynaForm> result = dynaFormRepository.findById(id);
        DynaForm dynaForm = null;
        if (result.isPresent()) {
            dynaForm = result.get();
        } else {
            throw new RuntimeException("Did not find the DynaForm id - " + id);
        }
        return dynaForm;
    }

    @Override
    public DynaForm save(DynaForm object) {
        return dynaFormRepository.save(object);
    }

    @Override
    public void delete(DynaForm object) {
        dynaFormRepository.delete(object);
    }

    @Override
    public void deleteById(String s) {
        dynaFormRepository.deleteById(s);
    }
}
