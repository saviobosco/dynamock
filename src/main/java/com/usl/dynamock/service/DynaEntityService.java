package com.usl.dynamock.service;

import com.usl.dynamock.data.DynaEntity;
import com.usl.dynamock.repository.DynaEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DynaEntityService implements CrudService<DynaEntity, String> {

    @Autowired
    DynaEntityRepository dynaEntityRepository;

    @Override
    public List<DynaEntity> findAll() {

        return dynaEntityRepository.findAll();
    }

    @Override
    public DynaEntity findById(String id) {

        Optional<DynaEntity> result = dynaEntityRepository.findById(id);
        DynaEntity dynaEntity = null;
        if (result.isPresent()) {
            dynaEntity = result.get();
        } else {
            throw new RuntimeException("Did not find the DynaEntity id - " + id);
        }
        return dynaEntity;
    }

    @Override
    public DynaEntity save(DynaEntity object) {
        return dynaEntityRepository.save(object);
    }

    @Override
    public void delete(DynaEntity object) {
        dynaEntityRepository.delete(object);
    }

    @Override
    public void deleteById(String id) {
        dynaEntityRepository.deleteById(id);
    }
}
