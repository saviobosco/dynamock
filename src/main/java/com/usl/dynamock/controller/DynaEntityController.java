package com.usl.dynamock.controller;

import com.usl.dynamock.data.DynaEntity;
import com.usl.dynamock.data.DynaField;
import com.usl.dynamock.repository.DynaFieldRepository;
import com.usl.dynamock.service.DynaEntityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/dyna-entities")
public class DynaEntityController {

    @Autowired
    DynaEntityService dynaEntityService;

    @Autowired
    DynaFieldRepository dynaFieldRepository;

    @Operation(summary = "Get all entities")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Entities",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DynaEntity.class)) }),
             })
    @GetMapping("/")
    public List<DynaEntity> index()
    {
        return dynaEntityService.findAll();
    }



    @Operation(summary = "Get an entity by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Entity",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DynaEntity.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<DynaEntity> show(@Parameter(description = "id of Entity to be searched")
                                               @PathVariable("id") String id)
    {
        Optional<DynaEntity> dynaEntity = Optional.ofNullable(dynaEntityService.findById(id));
        return dynaEntity.map(entity -> ResponseEntity.ok().body(entity)).orElseGet(() -> ResponseEntity.notFound().build());
    }



    /**
     * POST DATA SCHEMA
     * {
     *     DynaEntity: {
     *         name: "Users",
     *         description: "Users tables",
     *         dynaFields: [
     *              {
     *                  "name": "firstName",
     *                  "type": "text"
     *              },
     *              {
     *                  "name": "lastName",
     *                  "type": "text"
     *              }
     *         ]
     *     }
     * }
     * @return String
     */
    @Operation(summary = "create a new entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The created Entity",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DynaEntity.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid post request supplied",
                    content = @Content),
            })
    @PostMapping("/")
    public ResponseEntity<Object> store(
            //final @RequestBody @Valid DynaEntityValidation dynaEntity,
                        @RequestBody DynaEntity dynaEntity,
                        final BindingResult bindingResult)
    {
        // make all fields camel cased, with underscores as field space replacement.
        List<DynaField> dynaFields = dynaEntity.getDynaFields();

        // clear out all dynaFields in the dynaEntity;
        dynaEntity.clearDynaFields();

        if (dynaFields.size() > 0) {
            for (int i = 0; i < dynaFields.size(); i++) {
                dynaEntity.addField(dynaFieldRepository.save(dynaFields.get(i)));
            }
        }
        DynaEntity newDynaEntity = dynaEntityService.save(dynaEntity);
        if (newDynaEntity == null) {
            ResponseEntity.notFound().build();
        } else {
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(newDynaEntity.getId())
                    .toUri();
            return ResponseEntity.created(uri)
                    .body(newDynaEntity);
        }
        return ResponseEntity.noContent().build();
    }


    @PutMapping("/{id}")
    public String update()
    {
        // update the details here...
        return "dyna_entities/edit";
    }


    @Operation(summary = "Delete an entity by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Deleted the Entity"),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> destroy(@PathVariable String id)
    {
        dynaEntityService.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
