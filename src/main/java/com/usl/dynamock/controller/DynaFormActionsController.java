package com.usl.dynamock.controller;

import com.usl.dynamock.enums.DynaFormActionColors;
import com.usl.dynamock.enums.DynaFormActionMethods;
import com.usl.dynamock.enums.DynaFormActionTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@RestController
@RequestMapping("/api/v1/form-actions")
public class DynaFormActionsController {

    @GetMapping("/types/list")
    public List<DynaFormActionTypes> getFormActionTypes()
    {
        return new ArrayList<DynaFormActionTypes>(EnumSet.allOf(DynaFormActionTypes.class));
    }

    @GetMapping("/methods/list")
    public List<DynaFormActionMethods> getFormActionMethods()
    {
        return new ArrayList<DynaFormActionMethods>(EnumSet.allOf(DynaFormActionMethods.class));
    }

    @GetMapping("/colors/list")
    public List<DynaFormActionColors> getFormActionColors()
    {
        return new ArrayList<DynaFormActionColors>(EnumSet.allOf(DynaFormActionColors.class));
    }


}
