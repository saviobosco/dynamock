package com.usl.dynamock.controller;

import com.usl.dynamock.data.DynaForm;
import com.usl.dynamock.enums.DynaFieldTypes;
import com.usl.dynamock.repository.DynaFormActionRepository;
import com.usl.dynamock.repository.DynaFormFieldRepository;
import com.usl.dynamock.repository.DynaFormSectionRepository;
import com.usl.dynamock.service.DynaFormService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@RestController
@RequestMapping("api/v1/dyna-forms")
public class DynaFormController {

    @Autowired
    DynaFormService dynaFormService;

    @Autowired
    DynaFormSectionRepository dynaFormSectionRepository;

    @Autowired
    DynaFormFieldRepository dynaFormFieldRepository;

    DynaFormActionRepository dynaFormActionRepository;

    @GetMapping("/")
    public List<DynaForm> list()
    {
        return null;
    }

    /**
     * POST DATA FORMAT
     * {
     *     title: "User Registration Form",
     *     subTitle: "User reg form",
     *     dynaEntity: "Users Table",
     *     dynaFormSections: [{
     *         title: "User registration form,
     *         subTitle: "User reg form",
     *         dynaFormFields: [
     *          {
     *              dynaField: first_name,
     *              required: True,
     *              editable: True,
     *              title: "First Name",
     *              placeholder:"Enter First Name",
     *              hint: "User first name"
     *          },
     *          {
     *              dynaField: last_name,
     *              required: True,
     *              editable: True,
     *              title: "Last Name",
     *              placeholder:"Enter Last Name",
     *              hint: "User last name"
     *          }
     *         ],
     *     }],
     *     dynaFormActions:[
     *          {
     *              actionTitle: "Save User Information",
     *              colorScheme: PRIMARY, [PRIMARY, INFO, DANGER, DEFAULT] : options available,
     *              color: "#ffffff",
     *              actionType: SAVE_OR_UPDATE, [SAVE_OR_UPDATE, CALL_INT_SERVICE, CALL_EXT_SERVICE],
     *              actionUrl: "https://localhost:8000" ,
     *              actionMethod: POST, [POST, PUT, GET, DELETE ] : options available,
     *              actionService: "", link to an action section
     *
     *          }
     *     ]
     *
     * }
     *
     * @return
     */
    @PostMapping("/")
    public ResponseEntity<Object> create(@RequestBody Object object1)
    {
        // Process all the data
        DynaForm form = new DynaForm();
        BeanUtils.copyProperties(object1, form);


        return ResponseEntity.noContent().build();


        // saving the form sections
        /*List<DynaFormSection> sections = dynaForm.getSections();
        dynaForm.clearFormSections();
        // loop through all form sections
        if (sections.size() > 0) {
            for (int i = 0; i < sections.size(); i++) {
                DynaFormSection section = dynaFormSectionRepository.save(sections.get(i));
                // loop through the form fields in the form section
                List<DynaFormField> fields = section.getDynaFormFields();
                section.clearFormFields(); // clear the previous form fields..
                // saving the form fields
                if (fields.size() > 0) {
                    for (int j = 0; j < fields.size(); j++) {
                        section.addFormField(dynaFormFieldRepository.save(fields.get(j)));
                    }
                }
                dynaForm.addFormSection(section);
            }
        }

        // saving the form actions
        List<DynaFormAction> actions = dynaForm.getActions();
        dynaForm.clearFormActions();
        if (actions.size() > 0) {
            for (int i = 0; i < actions.size(); i++) {
                dynaForm.addFormAction(dynaFormActionRepository.save(actions.get(i)));
            }
        }
        DynaForm newForm = dynaFormService.save(dynaForm);

        if (newForm == null) {
            ResponseEntity.notFound().build();
        } else {
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(newForm.getId())
                    .toUri();
            return ResponseEntity.created(uri)
                    .body(newForm);
        }
        return ResponseEntity.noContent().build();*/
    }


    @GetMapping("/{id}")
    public ResponseEntity<Object> show(@PathVariable("id") String id)
    {

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> edit(@PathVariable("id") String id)
    {
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> destroy(@PathVariable("id") String id)
    {
        // Remove the entity
        return ResponseEntity.noContent().build();
    }

    public List<DynaFieldTypes> getListOfFormFieldTypes()
    {
        return new ArrayList<DynaFieldTypes>(EnumSet.allOf(DynaFieldTypes.class));
    }
}
